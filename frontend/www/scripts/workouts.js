async function fetchWorkouts(ordering) {
	let response = await sendRequest(
		"GET",
		`${HOST}/api/workouts/?ordering=${ordering}`
	);

	if (!response.ok) {
		throw new Error(`HTTP error! status: ${response.status}`);
	} else {
		let data = await response.json();

		let workouts = data.results;

		// Custom ordering for ratings
		if (ordering === "ratings") {
			workouts.sort((a, b) => {
				let totalA = 0;
				let totalB = 0;

				for (let i = 0; i < a.ratings.length; i++) {
					totalA += a.ratings[i].rating;
				}

				for (let i = 0; i < b.ratings.length; i++) {
					totalB += b.ratings[i].rating;
				}

				let avgA = totalA / a.ratings.length;
				let avgB = totalB / b.ratings.length;

				return avgB - avgA;
			});
		}

		let container = document.getElementById("div-content");
		workouts.forEach((workout) => {
			let templateWorkout = document.querySelector("#template-workout");
			let cloneWorkout = templateWorkout.content.cloneNode(true);

			let aWorkout = cloneWorkout.querySelector("a");
			aWorkout.href = `workout.html?id=${workout.id}`;

			let h5 = aWorkout.querySelector("h5");
			h5.textContent = workout.name;

			let localDate = new Date(workout.date);

			let table = aWorkout.querySelector("table");
			let rows = table.querySelectorAll("tr");
			rows[0].querySelectorAll("td")[1].textContent =
				localDate.toLocaleDateString(); // Date
			rows[1].querySelectorAll("td")[1].textContent =
				localDate.toLocaleTimeString(); // Time
			rows[2].querySelectorAll("td")[1].textContent = workout.owner_username; //Owner
			rows[3].querySelectorAll("td")[1].textContent =
				workout.exercise_instances.length; // Exercises

			// Calculate workout rating average
			let totalRating = 0;
			for (let i = 0; i < workout.ratings.length; i++) {
				totalRating += workout.ratings[i].rating;
			}
			rows[4].querySelectorAll("td")[1].textContent =
				totalRating / workout.ratings.length; // Average rating

			// Create label string
			let labels = "";
			for (let i = 0; i < workout.labels.length; i++) {
				labels += workout.labels[i].name;
				if (i !== workout.labels.length - 1) {
					labels += ", ";
				}
			}

			rows[5].querySelectorAll("td")[1].textContent = labels; // Workout labels

			container.appendChild(aWorkout);
		});
		return workouts;
	}
}

function createWorkout() {
	window.location.replace("workout.html");
}

function getSelectValues(select) {
  var result = [];
  var options = select && select.options;
  var opt;

  for (var i=0; i<options.length; i++) {
    opt = options[i];

    if (opt.selected) {
      result.push(opt.value || opt.text);
    }
  }
  return result;
}

async function filterByLabels() {
	let labelInput = document.getElementById("inputLabels");
	let selectedLabels = getSelectValues(labelInput);
	
	let ordering = "-date";
	const urlParams = new URLSearchParams(window.location.search);
	if (urlParams.has("ordering")) {
		ordering = urlParams.get("ordering");
	}

	let response = await sendRequest(
		"GET",
		`${HOST}/api/workouts/?ordering=${ordering}`
	);
	let workoutsData = await response.json();
	let workouts = workoutsData.results;

	let workoutAnchors = document.querySelectorAll(".workout");
	for (let i = 0; i < workouts.length; i++) {
		// I'm assuming that the order of workout objects matches
		// the other of the workout anchor elements. They should, given
		// that I just created them.
		let workout = workouts[i];
		let workoutAnchor = workoutAnchors[i];
		
		let shouldHide = true && selectedLabels.length > 0;
		for (let j = 0; j < workout.labels.length; j++) {	
			if (selectedLabels.includes(workout.labels[j].name)){
				shouldHide = false;
			}
		}
		if (shouldHide) {
			workoutAnchor.classList.add("hide");
		}else {
			workoutAnchor.classList.remove("hide");
		}
	}
}

window.addEventListener("DOMContentLoaded", async () => {
	let createButton = document.querySelector("#btn-create-workout");
	createButton.addEventListener("click", createWorkout);
	let ordering = "-date";

	const urlParams = new URLSearchParams(window.location.search);

	let labelInput = document.querySelector("#inputLabels");
	labelInput.addEventListener("click", filterByLabels);
	setLabelsForm()
	if (urlParams.has("ordering")) {
		let aSort = null;
		ordering = urlParams.get("ordering");
		if (
			ordering == "name" ||
			ordering == "owner" ||
			ordering == "date" ||
			ordering == "rating"
		) {
			let aSort = document.querySelector(`a[href="?ordering=${ordering}"`);
			aSort.href = `?ordering=-${ordering}`;
		}
	}

	let currentSort = document.querySelector("#current-sort");
	currentSort.innerHTML =
		(ordering.startsWith("-") ? "Descending" : "Ascending") +
		" " +
		ordering.replace("-", "");

	let currentUser = await getCurrentUser();
	// grab username
	if (ordering.includes("owner")) {
		ordering += "__username";
	}
	let workouts = await fetchWorkouts(ordering);

	let tabEls = document.querySelectorAll('a[data-bs-toggle="list"]');
	for (let i = 0; i < tabEls.length; i++) {
		let tabEl = tabEls[i];
		tabEl.addEventListener("show.bs.tab", function (event) {
			let workoutAnchors = document.querySelectorAll(".workout");
			for (let j = 0; j < workouts.length; j++) {
				// I'm assuming that the order of workout objects matches
				// the other of the workout anchor elements. They should, given
				// that I just created them.
				let workout = workouts[j];
				let workoutAnchor = workoutAnchors[j];

				switch (event.currentTarget.id) {
					case "list-my-workouts-list":
						if (workout.owner == currentUser.url) {
							workoutAnchor.classList.remove("hide");
						} else {
							workoutAnchor.classList.add("hide");
						}
						break;
					case "list-athlete-workouts-list":
						if (
							currentUser.athletes &&
							currentUser.athletes.includes(workout.owner)
						) {
							workoutAnchor.classList.remove("hide");
						} else {
							workoutAnchor.classList.add("hide");
						}
						break;
					case "list-public-workouts-list":
						if (workout.visibility == "PU") {
							workoutAnchor.classList.remove("hide");
						} else {
							workoutAnchor.classList.add("hide");
						}
						break;
					default:
						workoutAnchor.classList.remove("hide");
						break;
				}
			}
		});
	}
});

async function setLabelsForm() {
	let labelInput = document.querySelector("#inputLabels");
	let labelsResponse = await sendRequest("GET", `${HOST}/api/workout-labels/`);
	let labelsData = await labelsResponse.json();
	let labelsResults = labelsData.results;

	for (let i = 0; i < labelsResults.length; i++) {
		// Add label to select element
		let option = document.createElement("option");
		option.value = labelsResults[i].name;
		option.innerHTML = labelsResults[i].name;

		labelInput.appendChild(option);
	}
}