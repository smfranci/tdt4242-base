const {
  sendRequest,
  inputRegisterForm,
} = require("../support/black-box-scripts");

const waitTime = 1000;
let existingUsernames = [];
// Valid (ascii 65), invalid (ascii 30 or length>50)
let invalid_string_length =
  "vF4X8xGHhwgi3s1AqLPTUo4PQY3yAZvuAyBPAsQEnhV3Zqi81EX9jFK";
let valid_invalid_inputs_username = ["username", "A", "▲"];
let valid_invalid_inputs_email = ["email", "A", "▲"];
let valid_invalid_inputs_password = ["password", "AAAA", "A"];
let valid_invalid_inputs_repeatPassword = ["repeatPassword", "AAAAA", "A"];
let valid_invalid_inputs_phone = ["phone", "A", invalid_string_length];
let valid_invalid_inputs_country = ["country", "A", invalid_string_length];
let valid_invalid_inputs_city = ["city", "A", invalid_string_length];
let valid_invalid_inputs_street = ["street", "A", invalid_string_length];

// For easy access
let array_of_valid_invalid_inputs = [
  valid_invalid_inputs_username,
  valid_invalid_inputs_email,
  valid_invalid_inputs_password,
  valid_invalid_inputs_repeatPassword,
  valid_invalid_inputs_phone,
  valid_invalid_inputs_country,
  valid_invalid_inputs_city,
  valid_invalid_inputs_street,
];

// Function for taking two arrays of input and testing the combination of those inputs
const testCombination = (array1, array2, comboPassword = false) => {
  const positions = [
    "username",
    "email",
    "password",
    "repeatPassword",
    "phone",
    "country",
    "city",
    "street",
  ];
  let position1 = positions.indexOf(array1.shift());
  let position2 = positions.indexOf(array2.shift());
  let testPassword = false;
  if (position1 === 2 || position2 === 2 || position1 === 3 || position2 === 3)
    testPassword = true;

  // All valid inputs
  let inputs = ["A", "A", "AAAA", "AAAA", "A", "A", "A", "A"];

  const executeTest = (inputs, testPassword, i, j, comboPassword) => {
    inputs[position1] = array1[i];
    inputs[position2] = array2[j];

    // If not testing 2-way between both password fields
    // -> make sure repeatpassword and password are the same
    if (!comboPassword) {
      if (position1 === 2) {
        inputs[position1 + 1] = inputs[position1];
      } else if (position1 === 3) {
        inputs[position1 - 1] = inputs[position1];
      }
      if (position2 === 2) {
        inputs[position2 + 1] = inputs[position2];
      } else if (position2 === 3) {
        inputs[position2 - 1] = inputs[position2];
      }
    }
    inputRegisterForm(...inputs, testPassword);
  };

  for (let i = 0; i < array1.length; i++) {
    for (let j = 0; j < array2.length; j++) {
      if (i === 1 || j === 1) {
        it(`invalid: ${array1[i]}, ${array2[j]}`, () => {
          i == 1 && j == 1 && cy.wait(waitTime * 5);
          executeTest(inputs, testPassword, i, j, comboPassword);
          // Should not create an account with invalid inputs
          cy.url().should("include", "/register.html");
        });
      } else {
        it(`valid: ${array1[i]}, ${array2[j]}`, () => {
          executeTest(inputs, testPassword, i, j, comboPassword);
          // Should create an account correctly and redirect to new URL
          cy.url().should("include", "/workouts.html");
        });
      }
    }
  }
};

// Function for testing 2-way combination of all the input fields
const test_all_combinations = () => {
  // Since we mutate the array in the loop, the outer loop length must be defined here
  let length = array_of_valid_invalid_inputs.length;

  for (let i = 0; i < length; i++) {
    let currentTest = array_of_valid_invalid_inputs.shift();

    context(currentTest[0], () => {
      for (let j = 0; j < array_of_valid_invalid_inputs.length; j++) {
        let testAgainst = array_of_valid_invalid_inputs[j];
        let comboPassword =
          currentTest[0] === "password" && testAgainst[0] === "repeatPassword";

        context(testAgainst[0], () => {
          testCombination([...currentTest], [...testAgainst], comboPassword);
        });
      }
    });
  }
};

// 2-way domain test of the register page
describe("2-way domain tests: register", () => {
  before(() => {
    // Logic for getting existing users
    // so we can delete the ones created by the test

    sendRequest("GET", "http://localhost:8000/api/users").then((response) =>
      response
        .json()
        .then(
          (body) =>
            (existingUsernames = body.results.map((user) => user.username))
        )
    );
  });

  afterEach(() => {
    // Logic for deleting users created by the test
    let addedUsernames = [];
    sendRequest("GET", "http://localhost:8000/api/users").then((response) =>
      response.json().then((body) => {
        console.log(body)
        addedUsernames = body.results.map((user) => user.username);
        addedUsernames.forEach((username) => {
          if (!existingUsernames.includes(username)) {
            sendRequest(
              "DELETE",
              `http://127.0.0.1:8000/api/users/${username}/`
            );
          }
        });
      })
    );
    cy.wait(waitTime)
  });

  // Run the test
  test_all_combinations();
});
