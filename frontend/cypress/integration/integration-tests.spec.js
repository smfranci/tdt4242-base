const { login, sendRequest } = require("../support/scripts");

let workouts = null;
const workout = {
	name: "Workout Test",
	date: "2022-06-01T08:30",
	notes: "Workout Test Notes",
	labels: ["Cardio", "Intervals"],
};
const user = {
	username: "group20",
	password: "group20",
};

// Integration tests for all new implemented features, and the components they include
describe("Integration tests", () => {
	// Code to run before all the tests
	before(async () => {
		// Login user
		await login(user.username, user.password);

		// Fetch all existing workouts for future reference
		const response = await sendRequest(
			"GET",
			"http://localhost:8000/api/workouts"
		);
		const body = await response.json();
		workouts = body.results;
	});

	// Code to run before each test is executed
	beforeEach(async () => {
		// Login user
		await login(user.username, user.password);
	});

	// Testing workout list, to see if labels and ratings is displayed properly
	context("Exercise list - (Test labels & ratings", () => {
		// Visit workouts page
		it("Visit workouts page successfully", () => {
			cy.visit("/workouts.html"); // Visit workouts page
			cy.url().should("include", "/workouts.html"); // Check if page was visited successfully
		});

		// Workouts is displayed successfully with the correct workouts count
		it("Workouts is displayed successfully", () => {
			cy.wait(500);
			cy.get("#div-content")
				.find("table")
				.should("have.length", workouts.length);
		});

		// Workouts display labels
		it("Labels are displayed if workout contains labels", () => {
			// Loop through all workouts
			for (let i = 0; i < workouts.length; i++) {
				// Fetch workout labels
				const labels = workouts[i].labels;
				let labelsString = "";

				// Generate labels string
				for (let j = 0; j < labels.length; j++) {
					labelsString += labels[j].name;

					if (j !== labels.length - 1) {
						labelsString += ", ";
					}
				}

				// Check if workout labels string exist in workouts

				cy.get("table").then((el) => {
					// Fetch workout labels from given table
					const workoutRows = el[i].children[0];
					const workoutRow = workoutRows.children[5];
					const workoutLabels = workoutRow.children[1].innerHTML;

					if (labelsString.length > 0) {
						expect(workoutLabels).to.equal(labelsString);
					} else {
						expect(workoutLabels.length).to.equal(0);
					}
				});
			}
		});

		// Workouts display rating
		it("Rating is displayed on workouts", () => {
			// Loop through all workouts
			for (let i = 0; i < workouts.length; i++) {
				// Fetch workout ratings
				const ratings = workouts[i].ratings;
				let ratingsTotal = 0;

				// Find total ratings for workout
				for (let j = 0; j < ratings.length; j++) {
					ratingsTotal += ratings[j].rating;
				}

				const ratingsAvg = Math.floor(ratingsTotal / ratings.length);

				cy.get("table").then((el) => {
					// Fetch workout rating from given table
					const workoutRows = el[i].children[0];
					const workoutRow = workoutRows.children[4];
					const workoutAvgRating = workoutRow.children[1].innerHTML;

					expect(workoutAvgRating).to.equal(ratingsAvg.toString());
				});
			}
		});

		// Workout sort by rating
		it("Sorts by rating successfully", () => {
			// Click sort by rating link
			cy.get("#sortByRating").click();

			let ratingsArray = [];
			let workoutsIsNotSorted = false;

			// Loop through workouts
			for (let i = 0; i < workouts.length; i++) {
				// Loop through all table objects containing workouts
				cy.get("table").then((el) => {
					// Fetch workout rating from given table
					const workoutRows = el[i].children[0];
					const workoutRow = workoutRows.children[4];
					let workoutAvgRating = workoutRow.children[1].innerHTML;

					if (workoutAvgRating === "NaN") workoutAvgRating = -1;
					else workoutAvgRating = Number(workoutAvgRating);

					ratingsArray.push(workoutAvgRating);
				});
			}

			// Check if workouts were sorted by rating
			for (let i = 0; i < ratingsArray.length; i++) {
				if (i < ratingsArray.length - 1) {
					// Check workout after workout has a larger rating
					if (ratingsArray[i] < ratingsArray[i + 1]) {
						// Set workoutIsNotSorted to true to throw an error
						workoutsIsNotSorted = true;
					}
				}
			}

			// Throw error if workouts is not sorted
			if (workoutsIsNotSorted) {
				cy.contains("Workouts were not sorted by rating successfully");
			} else {
				cy.contains("Workouts sorted successfully").should("not.exist");
			}
		});
	});

	// Testing creation of workout with labels
	context("Create workout - (Test labels & ratings)", () => {
		// Workout form displays after button click
		it("Workout form is displayed after button click", () => {
			cy.get("#btn-create-workout").click();
			cy.wait(500);
			cy.get("#form-workout").should("be.visible");
		});

		// Workout created successfully
		it("Workout is created successfully with labels", () => {
			cy.get("#inputName").should("be.visible").type(workout.name);
			cy.get("#inputDateTime").should("be.visible").type(workout.date);
			cy.get("#inputNotes").should("be.visible").type(workout.notes);
			cy.get("#inputLabels").should("be.visible").select(workout.labels);
			cy.get("#btn-ok-workout").click();

			// Check if an additional workout has been added
			cy.get("#div-content")
				.find("table")
				.should("have.length", workouts.length + 1);

			cy.get("table").then((el) => {
				// Fetch workout labels from given table
				const workoutRows = el[0].children[0];
				const workoutRow = workoutRows.children[5];
				const workoutLabels = workoutRow.children[1].innerHTML;

				expect(workoutLabels).to.equal("Cardio, Intervals");
			});
		});

		// Workout edit labels
		it("Workout edit labels", () => {
			cy.get("#workouts").click();
			cy.get("#btn-edit-workout").click();
			cy.get("#inputLabels").should("be.visible").select(workout.labels[0]);
			cy.get("#btn-ok-workout").click();
			cy.visit("/workouts.html");

			cy.get("table").then((el) => {
				// Fetch workout labels from given table
				const workoutRows = el[0].children[0];
				const workoutRow = workoutRows.children[5];
				const workoutLabels = workoutRow.children[1].innerHTML;

				expect(workoutLabels).to.equal("Cardio");
			});
		});

		// Workout delete successfully
		it("Workout deleted successfully", () => {
			cy.get("#workouts").click();
			cy.get("#btn-edit-workout").click();
			cy.get("#btn-delete-workout").click();

			// Check if the workout was deleted successfully
			cy.get("#div-content")
				.find("table")
				.should("have.length", workouts.length);
		});
	});
});
