const { login } = require("../support/scripts");

const athlete = {
	username: "AthleteTest",
	password: "password",
};

const coach = {
	username: "CoachTest",
	password: "password",
};

// FR5 testing
describe("FR5 tests", () => {
	// Testing coach workout views
	context("Coach workout views", () => {
		// Code to run before all the tests
		before(async () => {
			// Login user
			await login(coach.username, coach.password);
		});

		it("Coach can view Athlete's public workouts", () => {
			cy.visit("/workouts.html");
			cy.wait(500);
			cy.get("#div-content")
				.should("be.visible")
				.contains("Athlete Workout (Public)");
		});

		it("Coach can view Athlete's coach workouts", () => {
			cy.get("#div-content")
				.should("be.visible")
				.contains("Athlete Workout (Coach)");
		});

		it("Coach can't view Athlete's private workouts", () => {
			cy.get("#div-content").should("not.contain", "Athlete Workout (Private)");
		});

		it("Coach can view self-created public workouts", () => {
			cy.get("#div-content")
				.should("be.visible")
				.contains("Coach Workout (Public)");
		});

		it("Coach can view self-created private workouts", () => {
			cy.get("#div-content")
				.should("be.visible")
				.contains("Coach Workout (Private)");
		});

		it("Coach can view self-created coach workouts", () => {
			cy.get("#div-content")
				.should("be.visible")
				.contains("Coach Workout (Coach)");
		});
	});

	// Testing athlete workout views
	context("Athlete workout views", () => {
		// Code to run before all the tests
		before(async () => {
			// Login user
			await login(athlete.username, athlete.password);
		});

		it("Athlete can view Coach's public workouts", () => {
			cy.visit("/workouts.html");
			cy.wait(500);
			cy.get("#div-content")
				.should("be.visible")
				.contains("Coach Workout (Public)");
		});

		it("Athlete can't view Coach's private workouts", () => {
			cy.get("#div-content")
				.should("be.visible")
				.should("not.contain", "Coach Workout (Private)");
		});

		it("Athlete can't view Coach's coach workouts", () => {
			cy.get("#div-content")
				.should("be.visible")
				.should("not.contain", "Coach Workout (Coach)");
		});

		it("Athlete can view self-created public workouts", () => {
			cy.get("#div-content")
				.should("be.visible")
				.contains("Athlete Workout (Public)");
		});

		it("Athlete can view self-created private workouts", () => {
			cy.get("#div-content")
				.should("be.visible")
				.contains("Athlete Workout (Private)");
		});

		it("Athlete can view self-created coach workouts", () => {
			cy.get("#div-content")
				.should("be.visible")
				.contains("Athlete Workout (Coach)");
		});
	});
});
