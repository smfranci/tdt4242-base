const {
  login,
  sendRequest,
  inputExerciseForm,
  inputRegisterForm,
} = require("../support/black-box-scripts");

let existingExercisesId = [];
const waitTime = 1000;

// Boundary value testing for view/edit workout
describe("Boundary value testing: view/edit workout", () => {
  beforeEach(() => {
    // login
    login().then(() =>
      // Logic for getting existing exercises
      // so we can delete the ones created by the test
      sendRequest("GET", "http://localhost:8000/api/exercises").then(
        (response) =>
          response
            .json()
            .then(
              (body) =>
                (existingExercisesId = body.results.map(
                  (exercise) => exercise.id
                ))
            )
      )
    );
  });

  afterEach(() => {
    // Logic for deleting exercises created by the test
    let addedExercisesId = [];
    sendRequest("GET", "http://localhost:8000/api/exercises").then((response) =>
      response.json().then((body) => {
        addedExercisesId = body.results.map((exercise) => exercise.id);
        addedExercisesId.forEach((exerciseId) => {
          if (!existingExercisesId.includes(exerciseId)) {
            sendRequest(
              "DELETE",
              `http://127.0.0.1:8000/api/exercises/${exerciseId}/`
            );
          }
        });
      })
    );
    cy.wait(waitTime)
  });

  context("duration", () => {
    it("invalid", () => {
      // nom unit, invalid duration, nom calories
      inputExerciseForm("A", -1, 5);
      // Should not create an exercise correctly nor redirect to new URL
      cy.url().should("include", "/exercise.html");
    });

    it("boundary", () => {
      // nom unit, boundary duration, nom calories
      inputExerciseForm("A", 0, 5);
      // Should create an exercise correctly and redirect to new URL
      cy.url().should("include", "/exercises.html");
    });

    it("boundary+", () => {
      // nom unit, boundary+ duration, nom calories
      inputExerciseForm("A", 1, 5);
      // Should create an exercise correctly and redirect to new URL
      cy.url().should("include", "/exercises.html");
    });

    it("valid", () => {
      // nom unit, valid duration, nom calories
      inputExerciseForm("A", 5, 5);
      // Should create an exercise correctly and redirect to new URL
      cy.url().should("include", "/exercises.html");
    });
  });

  context("calories", () => {
    it("invalid", () => {
      // nom unit, nom duration, invalid calories
      inputExerciseForm("A", 5, -1);
      // Should not create an exercise correctly nor redirect to new URL
      cy.url().should("include", "/exercise.html");
    });

    it("boundary", () => {
      // nom unit, nom duration, boundary calories
      inputExerciseForm("A", 5, 0);
      // Should create an exercise correctly and redirect to new URL
      cy.url().should("include", "/exercises.html");
    });

    it("boundary+", () => {
      // nom unit, nom duration, boundary+ calories
      inputExerciseForm("A", 5, 1);
      // Should create an exercise correctly and redirect to new URL
      cy.url().should("include", "/exercises.html");
    });
  });

  context("unit", () => {
    it("invalid", () => {
      // invalid- unit, nom duration, nom calories
      inputExerciseForm("test▼ascii", 5, 5);
      // Should not create an exercise correctly nor redirect to new URL
      cy.url().should("include", "/exercise.html");
    });

    it("lower boundary", () => {
      // lower boundary unit (ascii 32), nom duration, nom calories
      inputExerciseForm("test ascii", 5, 5);
      // Should create an exercise correctly and redirect to new URL
      cy.url().should("include", "/exercises.html");
    });

    it("lower boundary+", () => {
      // lower boundary+ unit, nom duration, nom calories
      inputExerciseForm("test!ascii", 5, 5);
      // Should create an exercise correctly and redirect to new URL
      cy.url().should("include", "/exercises.html");
    });

    it("upper boundary-", () => {
      // upper boundary- unit, nom duration, nom calories
      inputExerciseForm("test}ascii", 5, 5);
      // Should create an exercise correctly and redirect to new URL
      cy.url().should("include", "/exercises.html");
    });

    it("upper boundary", () => {
      // upper boundary unit, nom duration, nom calories
      inputExerciseForm("test~ascii", 5, 5);
      // Should create an exercise correctly and redirect to new URL
      cy.url().should("include", "/exercises.html");
    });

    it("extended upper boundary-", () => {
      // extended upper boundary- unit, nom duration, nom calories
      inputExerciseForm("test²ascii", 5, 5);
      // Should create an exercise correctly and redirect to new URL
      cy.url().should("include", "/exercises.html");
    });

    it("upper boundary", () => {
      // extended upper boundary unit, nom duration, nom calories
      inputExerciseForm("test■ascii", 5, 5);
      // Should create an exercise correctly and redirect to new URL
      cy.url().should("include", "/exercises.html");
    });

    it("upper boundary+", () => {
      // extended upper boundary unit (ascii 255), nom duration, nom calories
      inputExerciseForm("test ascii", 5, 5);
      // Should not create an exercise correctly nor redirect to new URL
      cy.url().should("include", "/exercises.html");
    });
  });
});

// Function for testing the boundary values of a string input field
const testString = (input) => {
  const positions = [
    "username",
    "email",
    "password",
    "repeatPassword",
    "phone",
    "country",
    "city",
    "street",
  ];
  let position = positions.indexOf(input);
  let inputs = ["A", "A", "AAAA", "AAAA", "A", "A", "A", "A"];

  // Don't know if necessary to test
  it("invalid", () => {
    cy.wait(waitTime * 2);
    inputs[position] = "▼";
    inputRegisterForm(...inputs);
    // Should not create an account correctly nor redirect to new URL
    cy.url().should("include", "/register.html");
  });

  it("lower boundary", () => {
    inputs[position] = " ";
    // lower boundary unit (ascii 32), nom duration, nom calories
    inputRegisterForm(...inputs);
    // Should create an account correctly and redirect to new URL
    cy.url().should("include", "/workouts.html");
  });

  it("lower boundary+", () => {
    inputs[position] = "!";
    // lower boundary+ unit, nom duration, nom calories
    inputRegisterForm(...inputs);
    // Should create an account correctly and redirect to new URL
    cy.url().should("include", "/workouts.html");
  });

  it("upper boundary-", () => {
    inputs[position] = "}";
    // upper boundary- unit, nom duration, nom calories
    inputRegisterForm(...inputs);
    // Should create an account correctly and redirect to new URL
    cy.url().should("include", "/workouts.html");
  });

  it("upper boundary", () => {
    inputs[position] = "~";
    // upper boundary unit, nom duration, nom calories
    inputRegisterForm(...inputs);
    // Should create an account correctly and redirect to new URL
    cy.url().should("include", "/workouts.html");
  });

  it("extended upper boundary-", () => {
    //Should not run when testing email field
    if (!(input === "email")) {
      inputs[position] = " ";
      // extended upper boundary- unit, nom duration, nom calories
      inputRegisterForm(...inputs);
      // Should create an account correctly and redirect to new URL
      cy.url().should("include", "/workouts.html");
    }
  });

  it("extended upper boundary", () => {
    //Should not run when testing email field
    if (!(input === "email")) {
      inputs[position] = "■";
      // extended upper boundary unit, nom duration, nom calories
      inputRegisterForm(...inputs);
      // Should create an account correctly and redirect to new URL
      cy.url().should("include", "/workouts.html");
    }
  });
};

let existingUsernames = [];

// Boundary value testing for the register page
describe("Boundary value testing: register", () => {
  before(() => {
    // Logic for getting existing users
    // so we can delete the ones created by the test
    login().then(() =>
      sendRequest("GET", "http://localhost:8000/api/users").then((response) => {
        response
          .json()
          .then(
            (body) =>
              (existingUsernames = body.results.map((user) => user.username))
          );
      })
    );
  });

  afterEach(() => {
    // Logic for deleting users created by the test
    let addedUsernames = [];

    sendRequest("GET", "http://localhost:8000/api/users").then((response) =>
      response.json().then((body) => {
        addedUsernames = body.results.map((user) => user.username);
        addedUsernames.forEach((username) => {
          if (!existingUsernames.includes(username)) {
            sendRequest(
              "DELETE",
              `http://127.0.0.1:8000/api/users/${username}/`
            );
          }
        });
      })
    );
    cy.wait(waitTime)
  });

  context("username", () => {
    it("invalid", () => {
      let username = "▼";
      let inputs = [username, "A", "AAA", "AAA", "A", "A", "A", "A"];
      inputRegisterForm(...inputs);
      // Should not create an account correctly nor redirect to new URL
      cy.url().should("include", "/register.html");
    });

    it("special", () => {
      let username = "@.+-_1234567890";
      let inputs = [username, "A", "AAA", "AAA", "A", "A", "A", "A"];
      inputRegisterForm(...inputs);
      // Should  create an account correctly and redirect to new URL
      cy.url().should("include", "/workouts.html");
    });

    context("uppercase", () => {
      it("lower boundary", () => {
        let username = "A";
        let inputs = [username, "A", "AAA", "AAA", "A", "A", "A", "A"];
        inputRegisterForm(...inputs);
        // Should  create an account correctly and redirect to new URL
        cy.url().should("include", "/workouts.html");
      });

      it("lower boundary+", () => {
        let username = "B";
        let inputs = [username, "A", "AAA", "AAA", "A", "A", "A", "A"];
        inputRegisterForm(...inputs);
        // Should  create an account correctly and redirect to new URL
        cy.url().should("include", "/workouts.html");
      });

      it("upper boundary", () => {
        let username = "Z";
        let inputs = [username, "A", "AAA", "AAA", "A", "A", "A", "A"];
        inputRegisterForm(...inputs);
        // Should  create an account correctly and redirect to new URL
        cy.url().should("include", "/workouts.html");
      });

      it("upper boundary-", () => {
        let username = "Y";
        let inputs = [username, "A", "AAA", "AAA", "A", "A", "A", "A"];
        inputRegisterForm(...inputs);
        // Should  create an account correctly and redirect to new URL
        cy.url().should("include", "/workouts.html");
      });
    });

    context("lowercase", () => {
      it("lower boundary", () => {
        let username = "a";
        let inputs = [username, "A", "AAA", "AAA", "A", "A", "A", "A"];
        inputRegisterForm(...inputs);
        // Should  create an account correctly and redirect to new URL
        cy.url().should("include", "/workouts.html");
      });

      it("lower boundary+", () => {
        let username = "b";
        let inputs = [username, "A", "AAA", "AAA", "A", "A", "A", "A"];
        inputRegisterForm(...inputs);
        // Should  create an account correctly and redirect to new URL
        cy.url().should("include", "/workouts.html");
      });

      it("upper boundary", () => {
        let username = "z";
        let inputs = [username, "A", "AAA", "AAA", "A", "A", "A", "A"];
        inputRegisterForm(...inputs);
        // Should  create an account correctly and redirect to new URL
        cy.url().should("include", "/workouts.html");
      });

      it("upper boundary-", () => {
        let username = "y";
        let inputs = [username, "A", "AAA", "AAA", "A", "A", "A", "A"];
        inputRegisterForm(...inputs);
        // Should  create an account correctly and redirect to new URL
        cy.url().should("include", "/workouts.html");
        cy.wait(waitTime * 5);
      });
    });
  });

  context("email", () => {
    testString("email");
  });

  context("password", () => {
    it("invalid", () => {
      let password = "▼";
      let inputs = ["A", "A", password, password, "A", "A", "A", "A"];
      inputRegisterForm(...inputs);
      // Should not create an account correctly nor redirect to new URL
      cy.url().should("include", "/register.html");
    });

    it("boundary", () => {
      let password = "AA";
      let inputs = ["A", "A", password, password, "A", "A", "A", "A"];
      inputRegisterForm(...inputs);
      // Should create an account correctly and redirect to new URL
      cy.url().should("include", "/workouts.html");
    });

    it("boundary+", () => {
      let password = "AAA";
      let inputs = ["A", "A", password, password, "A", "A", "A", "A"];
      inputRegisterForm(...inputs);
      // Should create an account correctly and redirect to new URL
      cy.url().should("include", "/workouts.html");
      cy.wait(waitTime * 5);
    });
  });
  context("phone", () => {
    testString("phone");
  });

  context("country", () => {
    testString("country");
  });

  context("city", () => {
    testString("city");
  });

  context("street address", () => {
    testString("street");
  });
});
