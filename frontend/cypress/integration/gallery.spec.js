const { login, sendRequest } = require("../support/scripts");

const user = {
	username: "group20",
	password: "group20",
};

describe("AU testing for gallery.js", () => {
	// Code to run before every test
	beforeEach(async () => {
		await login(user.username, user.password); // Login user
	});
	context("AU testing for retrieveWorkoutImages()", () => {
		it("Invalid workout id, returns error", () => {
			cy.visit("/gallery.html?id=9999999");
			cy.get(".alert").should("be.visible");
		});

		it("Workout without images displays no images", () => {
			cy.visit("/gallery.html?id=3");
			cy.contains("This workout has no images.");
		});

		it("Workout with two images displays exactly two images", () => {
			cy.visit("/gallery.html?id=4");
			cy.get("#img-collection").find("img").should("have.length", 2);
		});

		it("Should click on image", () => {
			cy.visit("/gallery.html?id=4");
			cy.get("#img-collection")
				.find("img")
				.each((img, i) => {
					if (i === 1) {
						img.click();
						expect(img[0].attributes.style.value).to.equal("opacity: 0.6;");
					}
				});
		});

		it("Should delete image", () => {
			cy.visit("/gallery.html?id=1");
			cy.get("#img-collection-delete").find("input").click();
			cy.get("#img-collection").find("img").should("have.length", 0);
		});
	});
});
