const waitTime = 1000;

// Function for inputting into the exercise form using unit, duration and calories
export const inputExerciseForm = (unit, duration, calories) => {
  cy.visit("/exercise.html"); // change URL to match your dev URL
  cy.get("input[name=name]").type("testName");
  cy.get("textarea[name=description]").type("testDescription");
  cy.get("input[name=unit]").type(unit);
  cy.get("input[name=duration]").type(duration);
  cy.get("input[name=calories]").type(calories);
  cy.get("input[id=btn-ok-exercise]").click();

  cy.wait(waitTime);
};

// Function for inputting into all the fields of the registerform
export const inputRegisterForm = (
  username,
  email,
  password,
  repeatPassword,
  phone,
  country,
  city,
  street,
  testPasswordLength
) => {
  cy.visit("/register.html"); // change URL to match your dev URL
  cy.get("input[name=username]").type("TEST" + username + "ASCII");
  cy.get("input[name=email]").type("TEST" + email + "ASCII" + "@test.com");
  if (testPasswordLength) {
    cy.get("input[name=password]").type(password);
    cy.get("input[name=password1]").type(repeatPassword);
  } else {
    cy.get("input[name=password]").type("TEST" + password + "ASCII");
    cy.get("input[name=password1]").type("TEST" + repeatPassword + "ASCII");
  }
  cy.get("input[name=phone_number]").type("TEST" + phone + "ASCII");
  cy.get("input[name=country]").type("TEST" + country + "ASCII");
  cy.get("input[name=city]").type("TEST" + city + "ASCII");
  cy.get("input[name=street_address]").type("TEST" + street + "ASCII");
  cy.get("input[id=btn-create-account]").click();
  cy.wait(waitTime);
};

function setCookie(name, value, maxage, path = "") {
  document.cookie = `${name}=${value}; max-age=${maxage}; path=${path}`;
}

// Function for login into the website
export async function login() {
  let body = {
    username: "group20",
    password: "group20",
  };
  let response = await sendRequest(
    "POST",
    `http://127.0.0.1:8000/api/token/`,
    body
  );
  if (response.ok) {
    let data = await response.json();
    // access and refresh cookies each have a max age of 24 hours
    setCookie("access", data.access, 86400, "/");
    setCookie("refresh", data.refresh, 86400, "/");
    sessionStorage.setItem("username", "admin");
  }
}

// Function for getting the cookie value from local storage
export function getCookieValue(name) {
  let cookieValue = null;
  let cookieByName = document.cookie
    .split("; ")
    .find((row) => row.startsWith(name));

  if (cookieByName) {
    cookieValue = cookieByName.split("=")[1];
  }

  return cookieValue;
}

// Function for sending an request to a url
export async function sendRequest(
  method,
  url,
  body,
  contentType = "application/json; charset=UTF-8"
) {
  if (body && contentType.includes("json")) {
    body = JSON.stringify(body);
  }
  let myHeaders = new Headers();

  if (contentType) myHeaders.set("Content-Type", contentType);
  if (getCookieValue("access")) {
    myHeaders.set("Authorization", "Bearer " + getCookieValue("access"));
  }
  let myInit = { headers: myHeaders, method: method, body: body };
  let myRequest = new Request(url, myInit);
  //console.log(getCookieValue("access"));
  let response = await fetch(myRequest);
  if (response.status == 401 && getCookieValue("refresh")) {
    // access token not accepted. getting refresh token
    myHeaders = new Headers({
      "Content-Type": "application/json; charset=UTF-8",
    });
    let tokenBody = JSON.stringify({ refresh: getCookieValue("refresh") });
    myInit = { headers: myHeaders, method: "POST", body: tokenBody };
    myRequest = new Request(`http://127.0.0.1:8000/api/token/refresh/`, myInit);
    response = await fetch(myRequest);

    if (response.ok) {
      // refresh successful, received new access token
      let data = await response.json();
      setCookie("access", data.access, 86400, "/");

      let myHeaders = new Headers({
        Authorization: "Bearer " + getCookieValue("access"),
        "Content-Type": contentType,
      });
      let myInit = { headers: myHeaders, method: method, body: body };
      let myRequest = new Request(url, myInit);
      response = await fetch(myRequest);

      if (!response.ok) window.location.replace("logout.html");
    }
  }

  return response;
}
