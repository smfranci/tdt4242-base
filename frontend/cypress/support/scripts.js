async function login(username, password) {
	let body = {
		username: username,
		password: password,
	};
	let response = await sendRequest(
		"POST",
		`http://127.0.0.1:8000/api/token/`,
		body
	);
	if (response.ok) {
		let data = await response.json();
		// access and refresh cookies each have a max age of 24 hours
		setCookie("access", data.access, 86400, "/");
		setCookie("refresh", data.refresh, 86400, "/");
		sessionStorage.setItem("username", username);
	}
}

function setCookie(name, value, maxage, path = "") {
	document.cookie = `${name}=${value}; max-age=${maxage}; path=${path}`;
}

function getCookieValue(name) {
	let cookieValue = null;
	let cookieByName = document.cookie
		.split("; ")
		.find((row) => row.startsWith(name));

	if (cookieByName) {
		cookieValue = cookieByName.split("=")[1];
	}

	return cookieValue;
}

async function sendRequest(
	method,
	url,
	body,
	contentType = "application/json; charset=UTF-8"
) {
	if (body && contentType.includes("json")) {
		body = JSON.stringify(body);
	}
	let myHeaders = new Headers();

	if (contentType) myHeaders.set("Content-Type", contentType);
	if (getCookieValue("access")) {
		myHeaders.set("Authorization", "Bearer " + getCookieValue("access"));
	}
	let myInit = { headers: myHeaders, method: method, body: body };
	let myRequest = new Request(url, myInit);
	//console.log(getCookieValue("access"));
	let response = await fetch(myRequest);
	if (response.status == 401 && getCookieValue("refresh")) {
		// access token not accepted. getting refresh token
		myHeaders = new Headers({
			"Content-Type": "application/json; charset=UTF-8",
		});
		let tokenBody = JSON.stringify({ refresh: getCookieValue("refresh") });
		myInit = { headers: myHeaders, method: "POST", body: tokenBody };
		myRequest = new Request(`http://127.0.0.1:8000/api/token/refresh/`, myInit);
		response = await fetch(myRequest);

		if (response.ok) {
			// refresh successful, received new access token
			let data = await response.json();
			setCookie("access", data.access, 86400, "/");

			let myHeaders = new Headers({
				Authorization: "Bearer " + getCookieValue("access"),
				"Content-Type": contentType,
			});
			let myInit = { headers: myHeaders, method: method, body: body };
			let myRequest = new Request(url, myInit);
			response = await fetch(myRequest);

			if (!response.ok) window.location.replace("logout.html");
		}
	}

	return response;
}

module.exports = { login, sendRequest };
