# Run
REMEMBER! Use python 3.8.10 since the backend tests might fail to run otherwise.
### Start backend
<pre>
> cd .\backend\secfit\
> python -m venv venv
> venv/scripts/activate
> pip install -r requirements.txt
> python manage.py migrate
> python manage.py runserver
</pre>
### Start frontend
<pre>
> cd frontend
> npm install
> cordova run browser
</pre>
### Login
<pre>
> username=group20
> password=grou20
</pre>

# Tests
From here on we assume that you have run npm install in frontend, created venv in backend and installed requirements in the backend.
## Statement coverage Test
<pre>
> cd .\backend\secfit\
> python -m venv venv
> venv/scripts/activate
> python manage.py test users.tests // For serializer test
> python manage.py test workouts.tests // For permission test
</pre>
## Black-box Test
### Start backend
<pre>
> cd .\backend\secfit\
> python -m venv venv
> venv/scripts/activate
> python manage.py runserver
</pre>
### Start frontend
<pre>
> cd frontend
> cordova run browser --port=8001
</pre>
### Run tests
<pre>
> cd frontend
> cypress open
</pre>
