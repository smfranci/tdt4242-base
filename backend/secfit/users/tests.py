from io import BytesIO
from django.test import TestCase, LiveServerTestCase
from users.models import User
from django.http import QueryDict
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory
from django.core.files.uploadedfile import InMemoryUploadedFile
from users.serializers import (
    UserSerializer,
    AthleteFileSerializer,
    UserPutSerializer,
)

# Athletes dictionary
athletes = [{
    "username": "athlete1", "email": "email@gmail.com", "phone_number": "12345678",
    "country": "country", "city": "city", "street_address": "street_address",
    "password": "password123", "password1": "password123"
}, {
    "username": "athlete2", "email": "email@gmail.com", "phone_number": "12345678",
    "country": "country", "city": "city", "street_address": "street_address",
    "password": "password123", "password1": "password123"
}]

# User dictionary
user1 = {
    "username": "username", "email": "email@gmail.com", "phone_number": "12345678",
    "country": "country", "city": "city", "street_address": "street_address",
    "password": "password123", "password1": "password123"
}

user1_wrong_password = {
    "username": "username", "email": "email@gmail.com", "phone_number": "12345678",
    "country": "country", "city": "city", "street_address": "street_address",
    "password": "1", "password1": "1"
}

# Simulate an HTTP request
factory = APIRequestFactory()
request = factory.get('/')
serializer_context = {
    'request': Request(request),
    'format': None,
}


class UserSerializerTest (TestCase):
    def test_password(self):
        # Create a query_dict with the user1_wrong_password data which contains too short password
        query_dict = QueryDict('', mutable=True)
        query_dict.update(user1_wrong_password)

        # Create a serializer, passing in context and the user1_wrong_password data
        serializer = UserSerializer(
            data=query_dict, context=serializer_context)

        # The password is wrong, so it should not be valid
        self.assertEqual(serializer.is_valid(raise_exception=False), False)

    # Test if the UserSerializer works correctly
    def test_serialize(self):
        # Create a query_dict with the user1 data
        query_dict = QueryDict('', mutable=True)
        query_dict.update(user1)

        # Create a serializer, passing in context and the user1 data
        serializer = UserSerializer(
            data=query_dict, context=serializer_context)

        # Save the serializer and user object
        serializer.is_valid(raise_exception=True)
        serialized_user = serializer.save()

        # Check if the serializer created the object correctly
        self.assertEqual(user1['username'], serialized_user.username)
        self.assertEqual(user1['email'], serialized_user.email)
        self.assertEqual(user1['phone_number'], serialized_user.phone_number)
        self.assertEqual(user1['country'], serialized_user.country)
        self.assertEqual(user1['city'], serialized_user.city)
        self.assertEqual(user1['street_address'],
                         serialized_user.street_address)

        # The password should not be equal as it is encrypted in the serialized object
        self.assertNotEqual(user1['password'], serialized_user.password)

    # Test if the UserPutSerializer works correctly by updating a field
    def test_put_serialize(self):
        # Create the athlete (which has pk=1)
        athlete1_instance = User.objects.create(username=athletes[0]['username'], email=athletes[0]['email'], phone_number=athletes[0]['phone_number'],
                                                country=athletes[0]['country'], city=athletes[0]['city'], street_address=athletes[0]['street_address'], password=athletes[0]['password'])

        # Create the user1 instance
        user1_instance = User.objects.create(username=user1['username'], email=user1['email'], phone_number=user1['phone_number'],
                                             country=user1['country'], city=user1['city'], street_address=user1['street_address'], password=user1['password'])

        # Athletes refer to pk of other User objects
        user1athletes = {
            "athletes": athlete1_instance.id,
        }

        # Create a query_dict with the user1athletes data
        query_dict = QueryDict('', mutable=True)
        query_dict.update(user1athletes)

        # Create a UserPutSerializer, passing in context and the user1update data and the user instance
        serializer = UserPutSerializer(
            data=query_dict, context=serializer_context, instance=user1_instance)

        # Save the serializer
        serializer.is_valid(raise_exception=True)
        serialized_user = serializer.save()

        # Check if the athlete of the serialized object is correct
        for athlete in serialized_user.athletes.all():
            self.assertEqual(athlete1_instance,
                             athlete)

        # Check if the serializer did not alter other fields
        self.assertEqual(user1['username'], serialized_user.username)
        self.assertEqual(user1['email'], serialized_user.email)
        self.assertEqual(user1['country'], serialized_user.country)
        self.assertEqual(user1['city'], serialized_user.city)
        self.assertEqual(user1['street_address'],
                         serialized_user.street_address)
        self.assertEqual(user1['password'], serialized_user.password)


class UserSerializerTestLive (LiveServerTestCase):
    # Test if the AthleteFileSerializer create function works correctly
    def test_create_file_serialize(self):
        # Create the athlete (which has pk=1)
        athlete1_instance = User.objects.create(username=athletes[0]['username'], email=athletes[0]['email'], phone_number=athletes[0]['phone_number'],
                                                country=athletes[0]['country'], city=athletes[0]['city'], street_address=athletes[0]['street_address'], password=athletes[0]['password'])

        # Create the user1 instance
        user1_instance = User.objects.create(username=user1['username'], email=user1['email'], phone_number=user1['phone_number'],
                                             country=user1['country'], city=user1['city'], street_address=user1['street_address'], password=user1['password'])

        # Athlete file to be uploaded
        athlete_file = {
            "athlete": f"{self.live_server_url}/api/users/{athlete1_instance.id}/",
            "file": InMemoryUploadedFile(BytesIO(), None, "hello.jpg", "file_content", "image/jpeg", None, None),
        }
        # Create a query_dict with the athlete_file data
        query_dict = QueryDict('', mutable=True)
        query_dict.update(athlete_file)

        # Create a serializer, passing in context and the user1 data
        serializer = AthleteFileSerializer(
            data=query_dict, context=serializer_context)

        # Save the serializer and file object
        serializer.is_valid(raise_exception=True)
        serialized_file = serializer.save(owner=user1_instance)

        # Check if the serializer created the object correctly
        self.assertEqual(serialized_file.owner, user1_instance)
        self.assertEqual(serialized_file.athlete, athlete1_instance)

        # Check that the file is uploaded by comparing the name
        athlete_file_name = athlete_file["file"].name.replace(".jpg", "")
        serialized_file_name = serialized_file.file.name.replace(".jpg", "")
        self.assertIn(athlete_file_name, serialized_file_name)
