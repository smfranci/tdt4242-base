"""
Tests for the workouts application.
"""
import json
import numbers
from django.test import TestCase
from django.http import QueryDict
from django.test import Client
from django.utils import timezone
from rest_framework.request import Request
from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from django.http import HttpRequest

from users.models import User, Offer, AthleteFile
from workouts.models import Workout, Exercise, ExerciseInstance
from workouts.views import WorkoutDetail, ExerciseInstanceList, ExerciseInstanceDetail
from workouts.permissions import (IsOwner, IsOwnerOfWorkout, IsCoachAndVisibleToCoach,
                                  IsCoachOfWorkoutAndVisibleToCoach, IsPublic, IsWorkoutPublic, IsReadOnly)

# Dictionaries
user_template = {
    "username": "username",
    "email": "email@gmail.com",
    "phone_number": "12345678",
    "country": "country",
    "city": "city",
    "street_address": "street_address",
    "password": "password123",
    "password1": "password123"
}

athlete_template = {
    "username": "athlete",
    "email": "email@gmail.com",
    "phone_number": "12345678",
    "country": "country",
    "city": "city",
    "street_address": "street_address",
    "password": "password123",
    "password1": "password123"
}

workout_template = {
    "name": "Workout Test",
    "date": timezone.now(),
    "notes": "Workout Test Note",
    "visibility": "PU"
}

exercise_template = {
  "name": "Exercise Test",
  "description": "Exercise Test Description",
  "unit": "Shuu"
}

workout_view = WorkoutDetail.as_view()
exercise_view = ExerciseInstanceList.as_view()
exercise_instance_view = ExerciseInstanceDetail.as_view()


class WorkoutsPermissionsTest (TestCase):
    def setUp(self):
        """
          Setup data for testing.
        """
        # Create athlete
        self.athlete = User.objects.create(
            username=athlete_template['username'],
            email=athlete_template['email'],
            phone_number=athlete_template['phone_number'],
            country=athlete_template['country'],
            city=athlete_template['city'],
            street_address=athlete_template['street_address'],
            password=athlete_template['password']
        )

        # Create user
        self.user = User.objects.create(
            username=user_template['username'],
            email=user_template['email'],
            phone_number=user_template['phone_number'],
            country=user_template['country'],
            city=user_template['city'],
            street_address=user_template['street_address'],
            password=user_template['password'],
            coach=self.athlete
        )

        # Create workout with user as owner
        self.workout = Workout.objects.create(
            name=workout_template["name"],
            date=workout_template["date"],
            notes=workout_template["notes"],
            visibility=workout_template["visibility"],
            owner=self.user
        )

        # Create exercise
        self.exercise = Exercise.objects.create(
          name=exercise_template["name"],
          description=exercise_template["description"],
          unit=exercise_template["unit"]
        )

        # Create exercise instance with workout and exercise
        self.exercise_instance = ExerciseInstance.objects.create(
          workout=self.workout,
          exercise=self.exercise,
          sets=3,
          number=8
        )

        # Create coach offer
        self.offer = Offer.objects.create(
          owner=self.user,
          recipient=self.athlete,
          status="a"
        )

        self.factory = APIRequestFactory()
        self.client = APIClient()
        self.client.login(username=self.user.username, password=self.user.password)
        self.client.force_authenticate(user=self.user)


    def test_is_owner(self):
        """
         Test if user is owner of an existing object.
        """
        # Prepare HTTP request for workout instance
        request = self.factory.get('api/workouts/' + str(self.workout.id))
        force_authenticate(request, user=self.user)
        response = workout_view(request, pk=str(self.workout.id))

        # Test if response status code is successful
        self.assertEqual(response.status_code, 200)


    def test_is_owner_of_workout(self):
        """
          Test if user is owner of workout.
        """
        # Make HTTP request for testing has_permission()
        exercise_instance_post_response_good = self.client.post('http://testserver/api/exercise-instances/', {
          "exercise": "http://testserver/api/exercises/" + str(self.exercise.id) + "/",
          "workout": "http://testserver/api/workouts/" + str(self.workout.id) + "/",
          "sets": 3,
          "number": 8
        }, format="json")
        exercise_instance_post_response_bad_key = self.client.post('http://testserver/api/exercise-instances/', {
          "exercise": "http://testserver/api/exercises/" + str(self.exercise.id) + "/",
          "workouts": "http://testserver/api/workouts/" + str(self.workout.id) + "/",
          "sets": 3,
          "number": 8
        }, format="json")
        exercise_instance_get_response_good = self.client.get('http://testserver/api/exercise-instances/')
       
        # Make HTTP request for testing has_object_permission()
        request = self.factory.get('api/workouts/' + str(self.workout.id))
        force_authenticate(request, user=self.user)
        isOwnerOfWorkout = IsOwnerOfWorkout.has_object_permission(self, request=Request(request), view=exercise_view, obj=self.exercise_instance)
      
        self.assertEqual(isOwnerOfWorkout, True) # Test if user is owner of workout
        self.assertEqual(exercise_instance_post_response_good.status_code, 201) # Test if response status code is successful
        self.assertEqual(exercise_instance_get_response_good.status_code, 200) # Test if response status code is successful
        self.assertEqual(exercise_instance_post_response_bad_key.status_code, 403) # Test if response status code is forbidden


    def test_is_coach_and_visible_to_coach(self):
        """
          Test if user is the coach of the existing object's owner
          and if the workout has a visibility of Public or Coach.
        """
        self.client.login(username=self.athlete.username, password=self.athlete.password)
        self.client.force_authenticate(user=self.athlete)
        response = self.client.get('http://testserver/api/workouts/' + str(self.workout.id) + '/')

        self.assertEqual(response.status_code, 200) # Test if coach can view workout of student


    def test_is_coach_of_workout_and_visible_to_coach(self):
        """
          Test if user is the existing workout's owner's coach
          and if the workout has a visibility of Public or Coach.
        """
        self.client.login(username=self.athlete.username, password=self.athlete.password)
        self.client.force_authenticate(user=self.athlete)
        response = self.client.get('http://testserver/api/exercise-instances/' + str(self.exercise_instance.id) + '/')
        
        self.assertEqual(response.status_code, 200) # Test if coach can view exercise instance of student
        

    def test_is_public(self):
        """
          Test if the workout has visibility of Public.
        """
        request = self.factory.get('api/workouts/' + str(self.workout.id))
        force_authenticate(request, user=self.user)
        isPublic = IsPublic.has_object_permission(self, request=Request(request), view=workout_view, obj=self.workout)

        self.assertEqual(isPublic, True) # Test if workout is public
      

    def test_is_workout_public(self):
        """
          Test if the workout has visibility of Public.
        """
        request = self.factory.get('api/exercise-instances/' + str(self.exercise_instance.id))
        force_authenticate(request, user=self.user)
        isWorkoutPublic = IsWorkoutPublic.has_object_permission(self, request=Request(request), view=exercise_instance_view, obj=self.exercise_instance)

        self.assertEqual(isWorkoutPublic, True) # Test if workout is public


    def test_is_read_only(self):
        """
          Test if HTTP request only is read only (GET, HEAD, OPTIONS)
        """
        request = self.factory.get('api/workouts/' + str(self.exercise_instance.id) + '/')
        force_authenticate(request, user=self.athlete)
        isReadOnly = IsReadOnly.has_object_permission(self, request=Request(request), view=workout_view, obj=self.workout)

        self.assertEqual(isReadOnly, True) # Test if workout is read only

